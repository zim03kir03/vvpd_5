"""
Зимирев Кирилл КИ22 - 17/2Б

Написать программу, реализующую хранение информации о кредитах

(на основе списков и многоуровневых словарей).

Структура: ФИО (словарь с ключами: фамилия, имя, отчество), сумма,

процентная ставка, срок кредита.
"""

import pickle
import pathlib
from operator import itemgetter


def file_checking(indicators):
    """
    Функция нужна для сохранения информации в файле

    :param indicators: Список всей информации о заемщике
    :return:файл с информацией
    """
    with open('Заемщики.dat', 'wb') as file:
        pickle.dump(indicators, file)


def create_data(indicators):
    """
    Сохранение данных в двоичный файл
    Алгоритм сохраняет в двоичный файл, с именем указанным пользователем,
    информацию об объекте data

    :param indicators: объект, который будет сохранен
    :return: сохраненный объект
    """
    path = pathlib.Path('Заемщики.dat')
    if path.exists():
        with open('Заемщики.dat', 'rb') as file:
            indicators = pickle.load(file)
            print('Данные загружены')
            print(indicators)
        return indicators
    else:
        print('Файл не существует, создайте его !')


def add_people(indicators):
    """
    Алгоритм позволяет пользователю добавить новые объекты в словарь с данными

    Принимает у пользователя в одну строчку, через пробел, ФИО, сумму, процент и срок,
    ФИО служит ключем словаря, значением становится список, содержащий: словарь
    со значениями фамилии, имени и отчества; суммой; процентом; сроком.
    После, алгоритм автоматически готов к повторному вводу

    :param indicators: Словарь, который пользователь хочет пополнить
    :return: Измененный словарь
    """
    print('Введите данные о заемщике.')
    while True:
        check = True
        fio = input('Введите ФИО с пробелами:').split()
        for i in fio:
            if not i.isalpha():
                print('В ФИО допустимы только буквы.')
                check = False
                break
        if len(fio) != 3:
            print('Неверное количество данных.')
            check = False
        if not check:
            continue
        else:
            break
    summa = input('Введите сумма кредита: ')
    percent = input('Введите процентную ставку: ')
    term = input('Введите срок кредита: ')
    indicators.append([{'name': fio[0], 'surname': fio[1], 'otches': fio[2]}, summa, percent, term])
    return indicators


def del_in_file(indicators):
    """
    Алгоритм позволяет пользователю удалять объекты из словаря по ключу (ФИО)
    Принимает у пользователя с клавиатуры ФИО, объект с указанным ФИО будет удален.
    После, алгоритм автоматически готов к повторному удалению

    :param indicators: Словарь, который пользователь хочет изменить
    :return: Измененный словарь
    """
    delete = input('Введите ФИО заемщика через пробел').split()
    check_data = check_fio(delete, indicators)
    if check_data[0]:
        indicators.pop(check_data[1])
        print('Заёмщик удалён.')
    else:
        print('Вы ввели несуществующего заемщика.')


def find(indicators):
    """
    Поиск объекта по ФИО
    Алгоритм выводит на экран информацию об объекте,
    указанном пользователем по ФИО

    :param indicators: Словарь содержащий различные объекты
    :return: оюъект, которого вы запросили, если таковой есть
    """
    finder = input('Введите ФИО заемщика через пробел: ').split()
    check_data = check_fio(finder, indicators)
    if check_data[0]:
        print(indicators[check_data[1]])
    else:
        print('Такого заемщика не существует.')


def allowed_values():
    """
    Алгоритм запрашивает, а затем проверяет вводимое значение
    Алгоритм запрашивает, а затем проверяет, чтобы вводимое значение
    было целым числом, а также было больше параметра "min_mean" и
    меньше параметра "max_mean", функция не завершится,
    пока пользователь не введёт корректное значение

    :param: list
    :return: Корректное значение введённое пользователем
    """
    lists = input()
    if not lists.isdigit():
        print("Неверные данные !")
    elif int(lists) < 1 or int(lists) > 100:
        return False
    else:
        int(lists)


def filtering_by_date(old_list):
    """
    Фильтрация по сроку кредита

    :return: Словарь с заданным количеством заемщиков
    """
    new_list = old_list.copy()
    new_list.sort(key=itemgetter(3))
    if new_list:
        interested = int(input("Введите количество заемщиков для вывода: "))
        print(list[0:interested])
    else:
        print("Не удалось найти заемщиков, список пустой")


def interest_rate_filtering(indicators):
    """
    Алгоритм фильтрует объекты по годовому проценту
    Алгоритм передает значения словаря в список,
    фильтрует по указанному пользователем проценту

    :param indicators: Словарь содержащий различные объекты
    :return: Словарь из которого получали информацию для работы функции
    """
    storage = []
    while True:
        loan_rate = (input('Введите необходимую процентную ставку для фильтрации'))
        if loan_rate.isdigit():
            break
        else:
            print('Ошибка! Вы ввели не число.')
            continue
    for borrower in indicators:
        if borrower[2] == loan_rate:
            storage.append(borrower)
    if storage:
        print(storage)
    else:
        print('Заёмщиков с такой процентной ставкой нет')


def check_fio(fio, lists):
    """
    Поиск объекта по ФИО для получения информации
    Алгоритм выводит на экран информацию об объекте,
    указанном пользователем по ФИО

    :param: Словарь содержащий различные объекты
    :return: Словарь в котором осуществлялся поиск
    """
    for index in range(len(lists)):
        if lists[index][0]['name'] == fio[0]:
            return [True, index]
    return [False]


def show(lists):
    """
    Функция выводит всех заёмщиков на экран.
    Для удобства вывода.
    """
    for borrower in lists:
        print(borrower)


def main():
    """
    Основное тело программы
    Ожидает от пользователя ввода команд, а также: ключей к словарю и функциямий.
    Выполняет команды, по желанию выводит на экран список команд,
    завершается вводом "exit"
    """
    instructions = {
        'show':          ('Вывести всех заёмщиков на экран', show),
        'addition':      ('Добавление заемщиков', add_people),
        'removal':       ('Удаление заемщиков', del_in_file),
        'save_file':     ('Сохраняет ', file_checking),
        'create':        ('Загруженние информации', create_data),
        'search':        ('Поиск по ФИО', find),
        'interest_rate': ('Фильтрация по процентной ставке', interest_rate_filtering),
        'term':          ('Вывод определенного количества'
                          ' заемщиков с наибольшем сроком', filtering_by_date)
    }

    main_list = []
    for key, value in instructions.items():
        print(f'{value[0]} - {key}')
    print('Завершить работу программы, exit.')
    while True:

        answer = input('Введите команду.')

        if answer == 'create':
            main_list = instructions[answer][1]()
        elif answer == 'addition':
            main_list = instructions[answer][1](main_list)
        elif answer in instructions:
            instructions[answer][1](main_list)
        elif answer == 'exit':
            exit()
        else:
            print('Неверная команда.')


if __name__ == '__main__':
    """
    Точка входа в программе.
    """
    main()
